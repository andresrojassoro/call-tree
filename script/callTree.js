﻿var callTree = {
    selectedEmployee : {},
    employees: [],
    employeesByClient: [],
    rootEmployeeName:'',
    init: function () {
        this.readEmployeesXls();
        this.employees = JSON.parse(localStorage.getItem("employees"));
        this.employeesByClient = this.employees;
        this.rootEmployeeName = config.rootEmployeeName;
        this.printTree();
        this.events();
        this.clearLocalStorage();
        executiveInfo.init();
    },
    clearLocalStorage: function () {
        localStorage.clear();
    },
    events: function () {
        $('#clients').change(this.onClickSearchEmployee);
        $('#reportsTo').change(this.onClickRootCallTree);
        $('.menu_see').onClickSeeSection();
    },
    printTree: function () {
        this.addTopLevel();
        this.addChildren(this.rootEmployee.id);
    },
    readEmployeesXls: function () {
        var parseEmployees = function (data) {
            var list = data;
            var employees = [];
            var obj;
            var i = 0;
            for (i = 0; i < list.length; i++) {
                obj = {};
                obj.name = list[i][config.mapEmployees.name];
                obj.phone = list[i][config.mapEmployees.phone];
                obj.email = list[i][config.mapEmployees.email];
                obj.reportsTo = list[i][config.mapEmployees.reportsTo];
                obj.client = list[i][config.mapEmployees.client];
                employees.push(obj);
            }
            localStorage.setItem("employees", JSON.stringify(employees));
            callTree.readClients();
            callTree.readReportsTo();
        };
        var sql = 'SELECT * FROM XLSX("' + config.filePath + '",{headers:true}) ORDER BY [' + config.mapEmployees.name + '] ASC';
        alasql(sql, [], function (data) {
            parseEmployees(data);
        });
   
    },
    readClients: function () {
        var parseClients = function (data) {
            var list = data;
            var obj;
            var html;
            var i = 0;
            for (i = 0; i < list.length; i++) {
                html = "<option value='" + list[i].client + "'>" + list[i].client + "</option>"
                $('#clients').append(html);
            }
        };
        var sql = "SELECT DISTINCT client FROM ? WHERE client <> ' ' ORDER BY client ASC";
        alasql(sql, [callTree.employees], function (data) {
            parseClients(data);
        });
    },
    readReportsTo: function () {
        var parseReportsTo = function (data) {
            var list = data;
            var obj;
            var html;
            var i = 0;
            for (i = 0; i < list.length; i++) {
                if (list[i].reportsTo.trimStr() == callTree.rootEmployeeName.trimStr()) {
                    html = "<option value='" + list[i].reportsTo + "' selected>" + list[i].reportsTo + "</option>"
                }
                else {
                    html = "<option value='" + list[i].reportsTo + "'>" + list[i].reportsTo + "</option>"
                }
                $('#reportsTo').append(html);
            }
        };
        var sql = "SELECT DISTINCT reportsTo FROM ? WHERE reportsTo <> ' ' ORDER BY reportsTo ASC";
        alasql(sql, [callTree.employees], function (data) {
            parseReportsTo(data);
        });
    },
    addTopLevel: function () {
        $('ul[data-parentid="0"]').html('');
        var i = 0;
        var employee;
        for (i = 0; i < this.employees.length; i++) {
            employee = this.employees[i];
            employee.id = i;
            this.employees[i] = employee;
            if (this.employees[i].name.trimStr() == callTree.rootEmployeeName.trimStr()) {
                this.rootEmployee = this.employees[i];
                this.rootEmployee.parent = {};
                this.rootEmployee.parent.id = 0;
                this.addEmployeeRow(this.rootEmployee);
                $('li[data-id=' + this.rootEmployee.id + ']').attr("data-status", "open");
            }
        }
    },
    addChildren: function (id) {
        var parent = this.employees[id];
        var parentName = parent.name.trimStr();
        var html = document.createElement("ul");
        html.setAttribute("class", "department");
        var li;
        var ul;
        var i = 0;
        for (i = 0; i < callTree.employeesByClient.length; i++) {
            if (callTree.employeesByClient[i] != null && callTree.employeesByClient[i].reportsTo != null && callTree.employeesByClient[i].reportsTo.trimStr() == parentName) {
                if (parent.subemployees == null)
                {
                    parent.subemployees = [];
                }
                callTree.employeesByClient[i].parent = parent;
                parent.subemployees.push(this.employeesByClient[i]);
                callTree.employeesByClient[parent.id] = parent;
                callTree.addEmployeeRow(this.employeesByClient[i]);
            }
        }
    },
    addSubemployees: function (id) {
        $('#employee_subemployees').html("");
        var i;
        if (typeof this.employees[id].subemployees !== 'undefined') {
            for (i = 0; i < this.employees[id].subemployees.length; i++) {
                var html = document.createElement("li");
                html.innerHTML = this.employees[id].subemployees[i].name;
                $('#employee_subemployees').append(html);
            }
        }
    },
    addEmployeeRow: function (employee) {
        li = document.createElement("li");
        li.setAttribute("data-id", employee.id);
        li.setAttribute("data-status", "init");
        li.setAttribute("class", "noselect");
        var div = document.createElement("div");
        div.setAttribute("class", "treeNode");
        var close = document.createElement("span");
        close.setAttribute("class", "close");
        close.innerHTML = '&#10006;';
        var nodeName = document.createElement("p");
        var spanName = document.createElement("span");
        spanName.innerHTML = employee.name;
        spanName.setAttribute("class", "empName");
        var spanEmail = document.createElement("span");
        spanEmail.innerHTML = employee.email.trimStr();
        spanEmail.setAttribute("class", "empEmail");
        nodeName.appendChild(spanName);
        nodeName.appendChild(spanEmail);
       // nodeEmail.innerHTML = employee.email;
        var nodeCellPhone = document.createElement("p");
        nodeCellPhone.innerHTML = 'Phone: '+ employee.phone;
        nodeCellPhone.setAttribute('class', 'phone');
        div.appendChild(nodeName);
        div.appendChild(nodeCellPhone);
        li.appendChild(close);
        li.appendChild(div);
        ul = document.createElement("ul");
        ul.setAttribute("data-parentid", employee.id);
        ul.setAttribute("class","noselect");
        li.appendChild(ul);
        $('ul[data-parentid=' + employee.parent.id + ']').append(li);
        $('li[data-id=' + employee.id + '] div').onClickEmployeeTree();
        $('li[data-id=' + employee.id + '] .close').onClickHideNode();
    },
    onClickSearchEmployee: function () {
        var client = $('#clients').val();
        executiveInfo.clientEscalation(client);
        if (client == 0) {
            callTree.employeesByClient = callTree.employees;
        } else {
            callTree.employeesByClient = alasql('SELECT * FROM ? WHERE client = ?', [callTree.employees, client]);
        }
        $('ul[data-parentid=' + callTree.rootEmployee.id + ']').html("");
        callTree.addChildren(callTree.rootEmployee.id);
    },
    onClickRootCallTree : function(){
        var client = $('#reportsTo').val();
        callTree.rootEmployeeName = client;
        callTree.printTree();
    },
    print: function () {
        window.print();
    },
};
//Document Ready
$(document).ready(function () {
    callTree.init();
});
//JQuery Pluggin
(function ($) {
    $.fn.onClickEmployeeTree = function () {
        function onClick() {
            var id = $(this).parent().attr("data-id");
            if ($(this).parent().attr("data-status") == "init") {
                callTree.addChildren(id);
                $(this).parent().attr('data-status', 'open');
            } else if ($(this).parent().attr("data-status") == "open") {
                $('ul[data-parentid=' + id + ']').hide();
                $(this).parent().attr('data-status', 'closed');
            } else {
                $('ul[data-parentid=' + id + ']').show();
                $(this).parent().attr('data-status', 'open');
            }
        }
        return this.click(onClick);
    }
    $.fn.onClickHideNode = function () {
        function onClick() {
            $(this).parent().hide();
        }
        return this.click(onClick);
    }
    $.fn.onClickSeeSection = function () {
        function onClick() {
            var section = $(this).attr("data-section");
            $('section').hide();
            switch (section) {
                case "0":
                    $('section').show();
                    break;
                case "1":
                    $('.executive').show();
                    break;
                case "2":
                    $('.callTree').show();
                    break;
            }
        }
        return this.click(onClick);
    }
})(jQuery);
//String methods
String.prototype.trimStr = function trimStr() {
    return this.replace(/\s/g, "").toLowerCase();
};