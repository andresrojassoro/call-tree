﻿data = {
    executiveInfo: [
        { title: 'EXECUTIVE TEAM', list: ['Hedeby Rickard', 'Lage Bill', 'McLoud Mark', 'Falco Arthur', 'Feuerhelm Kent']},
        { title: 'DIRECTORS / FUNCTIONAL MANAGERS', list: ['DuPoux Jean Bernard', 'Bermudez Jeffry', 'Quintero Antonio Jose', 'Arce Gerardo', 'Bello Melania', 'Alvarado Karen', 'Solari Gina Maria', 'Mora Tatiana'] },
        { title: 'OPERATIONAL MANAGERS', list: ['Arguedas Dionisio'] },
    ],
    clientEscalation:[
        { title: 'HONEYWELL', client: ' Honeywell International, Inc.', employee: { name: 'Name', email: 'email@mail.com', phone: '(123)123-456-678' } },
        { title: 'VMC', client: ' VMC', employee: { name: 'Jon Atkinson', email: 'jatkinson@vmc.com', phone: '+1514.787.3175x7388[ [W], +1514.589.7241' } },
        { title: 'TRICORE', client: ' Rhodes Group', employee: { name: 'Steve Ayer', email: 'sayer@rhodesgroup.com', phone: '727-967-4059 [W]' } },
        { title: 'RODAN + FIELDS', client: ' Rodan + Fields, LLC', employee: { name: 'Prashanti Aduma', email: 'paduma@rodanandfields.com', phone: '510-676-7184' } },
        { title: 'MICROCHIP', client: ' Microchip Technology, Inc.', employee: { name: 'Richard Glaser', email: 'richard.glaser@microchip.com', phone: '480-792-8058' } },
        { title: 'CATLIN', client: ' Catlin Insurance Group, Inc.', employee: { name: 'David Ostermayer', email: 'David.Ostermayer@xlcatlin.com', phone: '404 348 5557[W] | 678 793 1150[C]' } },
        { title: 'WELLS FARGO', client: ' Wells Fargo Services Company', employee: { name: 'Monica L Guzman', email: 'Monica.Guzman@wellsfargo.com', phone: '480-404-5767' } },
    ]
}