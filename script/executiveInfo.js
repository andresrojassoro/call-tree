﻿executiveInfo = {
    operationalManagers : [],
    init: function () {
        this.executiveInfo();
    },
    executiveInfo: function () {
        var parseOpsManager = function (data) {
            executiveInfo = data;
            var i;
            var html='';
            for (i = 0; i < executiveInfo.length; i++) {
                html += '<h3>' + executiveInfo[i].title + '</h3>';
            }
           
        };
        $('#executiveInfo').html("");
        var html='';
        var sql='';
        var res;
        var i, j, k, l;
        var count;
        var obj;
        for (i = 0; i < data.executiveInfo.length; i++) {
            count = 0;
            obj = data.executiveInfo[i];
            html ='<h3>'+obj.title+'</h3>';
            sql = "SELECT * FROM ? WHERE name IN (";
            for (j = 0; j < obj.list.length; j++) {
                if (count > 0) {
                    sql += ",";
                }
                sql += "'" + obj.list[j] + "'";
                count++;
            }
            sql += ")";
            res = alasql(sql, [callTree.employees]);
            for (k = 0; k < res.length; k++) {
                html += executiveInfo.addExecutiveRow(res[k]);
            }
            $('#executiveInfo').append(html);
        }
    },
    addExecutiveRow : function(obj){
        return '<p><span class="execname">' + obj.name + '</span><span class="execemail">' + obj.email.trimStr() + '</span><span class="execPhone">' + obj.phone + '</span></p>';
    },
    clientEscalation: function (client) {
        var i;
        var html = '';
        for (i = 0; i < data.clientEscalation.length; i++)
        {
            if (data.clientEscalation[i].client == client)
            {
                html += '<h3>' + data.clientEscalation[i].title + '</h3>'
                html += executiveInfo.addExecutiveRow(data.clientEscalation[i].employee);
            }
        }
        document.getElementById('clientEscaltion').innerHTML = html;
    }
}